'use strict';

const insertVoteQuery = require('../../bbdd/queries/votes/insertVoteQuery');

const vote = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { value } = req.body;

        // Insertamos el nuevo post.
        await insertVoteQuery(value, id, req.user);

        res.send({
            status: 'ok',
            message: 'voto creado',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = vote;
