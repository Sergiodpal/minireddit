'use strict';

const updatePostQuery = require('../../bbdd/queries/posts/updatePostQuery');
const { generateError } = require('../../helpers');

const editPost = async (req, res, next) => {
  const { title, content } = req.body;

  try {
    if (!title && !content) {
      generateError('No se han introducido datos', 400);
    }

    await updatePostQuery(req.body, req.params.id, req.user);

    res.send({
      status: 'ok',
      data: {
        message: 'datos post actualizados',
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = editPost;
