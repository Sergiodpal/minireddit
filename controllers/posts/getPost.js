'use strict';

const selectPostByIdQuery = require('../../bbdd/queries/posts/selectPostByIdQuery');

const getPost = async (req, res, next) => {
    try {
        const { id } = req.params;

        // Info del post.
        const post = await selectPostByIdQuery(id);

        res.send({
            status: 'ok',
            data: {
                message: 'datos post',
                post,
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = getPost;
