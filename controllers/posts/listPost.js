'use strict';

const selectAllPostsQuery = require('../../bbdd/queries/posts/selectAllPostsQuery');

const listPost = async (req, res, next) => {
    try {
        // Lista de posts.
        const posts = await selectAllPostsQuery();

        res.send({
            status: 'ok',
            data: {
                message: 'lista post',
                posts,
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = listPost;
