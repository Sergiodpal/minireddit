'use strict';

const insertPostQuery = require('../../bbdd/queries/posts/insertPostQuery');
const { generateError } = require('../../helpers');

const newPost = async (req, res, next) => {
    try {
        const { title, content } = req.body;
        const tokenId = req.user;

        if (!title || !content) {
            generateError('Faltan campos', 400);
        }

        // Insertamos el nuevo post.
        await insertPostQuery(title, content, tokenId);

        res.send({
            status: 'ok',
            message: 'Post creado',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newPost;
