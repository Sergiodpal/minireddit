'use strict';

const updateUserQuery = require('../../bbdd/queries/users/updateUserQuery');
const { generateError } = require('../../helpers');

const editUser = async (req, res, next) => {
  const { name, email, password } = req.body;

  if (!name && !email && !password) {
    generateError('No se han introducido datos', 400);
  }

  try {
    await updateUserQuery(req.body, req.user);

    res.send({
      status: 'ok',
      data: {
        message: 'datos usuario actualizados',
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = editUser;
