'use strict';

const { generateError } = require('../helpers');
const getDB = require('../bbdd/getConnection');

const isPostAuthor = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const tokenId = req.user;

        const { id: postId } = req.params;

        const [postUserId] = await connection.query(
            `
            SELECT id_user FROM posts
            WHERE id=?
            `,
            postId
        );

        if (postUserId[0].id_user !== tokenId)
            generateError('No tienes permisos para editar este post', 403);

        next();
    } catch (err) {
        next(err);
    }
};
module.exports = isPostAuthor;
