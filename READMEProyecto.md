# Portal genérico de opiniones.

Implementar una API que permita publicar opiniones y que otras personas puedan verlas.

## Dependencias desarrollo

-   eslint // aviso de errores en el codigo
-   prettier // gestion de errores

## Dependencias producción

-   express // simulacion de servidor
-   nodemon // carga cambios automaticos en el servidor tras guardar
-   dotenv // acceso al archivo .env
-   morgan // imprime las peticiones en consola
-   mysql2 // da acceso a mysql para las tablas
-   jsonwebtoken // gestion del token json
-   bcrypt // encriptacion de datos sensibles

## Dependencias por implementar en Proy.3:

-   joi ? // comprueba que los datos instroducidos sean correctos, como un email con su estructura

## Instalar

-   Crear una base de datos vacía en una instancia de MySQL local.

-   Guardar el archivo `.env.example` como `.env` y cubrir los datos necesarios.

-   Ejecutar `npm run initDB` para crear las tablas necesarias en la base de datos anteriormente creada.

-   Ejecutar `npm run dev` o `npm start` para lanzar el servidor.

## Entidades

-   Users:

    -   id
    -   email
    -   password
    -   createdAt
    -   modifiedAt
    -   status

-   Post:

    -   id
    -   idUser
    -   text
    -   image (opcional)
    -   createdAt
    -   modifiedAt

-   Votos:

    -   id
    -   varchar check like dislike
    -   user_id
    -   tweet_id

## Endpoints

Usuarios:

-   POST [/user] - Registro de usuario✅ Requiere los campos name, email y password en body.
-   POST [/user/login] - Login de usuario (devuelve token en header) ✅ Requiere los campos email y password en body.
-   GET [/user/:id] - Devuelve información del usuario. Si el token coincide con el param, devuelve más datos. ✅
-   PUT [/user/edit] - modificar información del usuario. ✅ Requiere token en header. Requiere oldPwd en body. Voluntaria cualquier combinación de campos password, email y name.
-   PUT [/user/delete] Anonimiza usuario. ✅ Requiere token en header y password en body. Elimina token(logout).

Posts:

-   POST [/post] - Permite crear un post (necesita cabecera con token) ✅
-   GET [/post] - Lista todos los post mostrando un recuento total entre likes y dislikes ✅
-   GET [/post/:id] - Devuelve un post mostrando un recuento total entre likes y dislikes ✅
-   PUT [/post:id] - Modifica título y/o contenido de post. ✅ Requiere header con token.
-   DELETE [/post:id] - Elimina post y votos relacionados. ✅ Requiere header con token.

Votes:

-   POST [/post/:id/vote] - Permite votar. Si se ha votado previamente, escanea el voto anterior, y si es diferente lo modifica. ✅ Requiere header con token.
-   DELETE [post:id/vote] Elimina voto. ✅ Requiere header con token.

## Pendiente para proyecto 3:

-   Añadir la opción de postear una imagen + avatar en perfil.
-   Añadir JOI.
