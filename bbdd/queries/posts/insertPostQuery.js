'use strict';

const getDB = require('../../getConnection');

const insertPostQuery = async (title, content, token) => {
    let connection;

    try {
        connection = await getDB();

        await connection.query(
            `
                INSERT INTO posts (title, content, id_user)
                VALUES (?, ?, ?)
            `,
            [title, content, token]
        );
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertPostQuery;
