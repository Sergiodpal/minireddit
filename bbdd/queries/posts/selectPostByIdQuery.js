'use strict';

const getDB = require('../../getConnection');

const { generateError } = require('../../../helpers');

const selectPostByIdQuery = async (id) => {
    let connection;

    try {
        connection = await getDB();

        const [posts] = await connection.query(
            `SELECT p.id, p.createdAt, p.title, p.content, u.name, u.status,  
			(SELECT COUNT(*) FROM votes WHERE value = 'like' AND id_post = p.id) 
            - (SELECT COUNT(*) FROM votes WHERE value = 'dislike' AND id_post = p.id) AS likes
            FROM posts AS p
            LEFT JOIN users AS u ON (u.id = p.id_user)
			LEFT JOIN votes AS v ON (v.id_post = p.id)
            WHERE p.id = ?
			GROUP BY p.id
            ORDER BY p.createdAt DESC`,
            id
        );

        if (posts.length < 1) {
            generateError('Post no encontrado', 404);
        }

        const post = posts[0];

        if (post.status === 'deleted') {
            post.name = 'Usuario eliminado';
        }

        delete post.status;

        return post;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = selectPostByIdQuery;
