'use strict';

const getDB = require('../../getConnection');

const { generateError } = require('../../../helpers');

const selectUserByIdQuery = async (id, tokenId) => {
    let connection;

    try {
        connection = await getDB();
        let [users] = '';
        if (Number(id) === tokenId) {
            [users] = await connection.query(
                `SELECT id, status, name, email, createdAt FROM users WHERE id = ?`,
                [id]
            );
        } else {
            [users] = await connection.query(
                `SELECT id, status, name, createdAt FROM users WHERE id = ?`,
                [id]
            );
        }

        // Si el array de usuarios está vacío lanzo un error.
        if (users.length < 1) {
            generateError('Usuario no encontrado', 404);
        }
        const user = users[0];
        if (user.status === 'deleted') {
            delete user.id;
            user.name = 'usuario eliminado';
        }
        delete user.status;

        // Retornamos al usuario de la posición 0.
        return user;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = selectUserByIdQuery;
