'use strict';

const getDB = require('../../getConnection');
const bcrypt = require('bcrypt');
const { generateError } = require('../../../helpers');

const updateUserQuery = async (body, user) => {
  let connection;

  try {
    connection = await getDB();

    const { name, email, password } = body;

    const bodyNames = Object.keys(body).sort();

    const hashedPass = await bcrypt.hash(password, 10);

    let queryString = 'modifiedAt=?';
    let queryArray = [new Date()];
    for (const entry of bodyNames) {
      if (entry === 'name' || entry === 'email' || entry === 'password') {
        queryString += `, ${entry}=?`;
      }
      if (entry === 'name') queryArray.push(name);
      if (entry === 'email') queryArray.push(email);
      if (entry === 'password') queryArray.push(hashedPass);
    }
    queryArray.push(user);

    await connection.query(
      `
            UPDATE users
            SET ${queryString}
            WHERE id=?
            `,
      queryArray
    );
  } catch (error) {
    generateError('Error inesperado durante la solicitud', 500);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = updateUserQuery;
