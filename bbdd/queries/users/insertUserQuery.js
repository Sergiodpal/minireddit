'use strict';

const getDB = require('../../getConnection');
const bcrypt = require('bcrypt');

const { generateError } = require('../../../helpers');

const insertUserQuery = async (name, email, password) => {
    let connection;

    try {
        connection = await getDB();

        // Encriptamos la contraseña.
        const hashedPass = await bcrypt.hash(password, 10);

        // Insertamos el usuario.
        await connection.query(
            `INSERT INTO users (name, email, password) VALUES (?, ?, ?)`,
            [name, email, hashedPass]
        );
    } catch (err) {
        if (err.errno === 1062)
            generateError('Ya existe un usuario con esos datos', 409);
        throw err;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertUserQuery;
