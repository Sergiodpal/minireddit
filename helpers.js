'use strict';

const jwt = require('jsonwebtoken');

/**
 * ####################
 * ## Generate Error ##
 * ####################
 */

const generateError = (msg, code) => {
    const err = new Error(msg); // mensaje de error
    err.statusCode = code; // codigo de error
    throw err;
};

const getTokenValue = async (token) => {
    try {
        const tokenValue = jwt.verify(token, process.env.SECRET).id;
        return tokenValue;
    } catch (err) {
        const tokenValue = 'No token';
        return tokenValue;
    }
};
module.exports = {
    generateError,
    getTokenValue,
};
